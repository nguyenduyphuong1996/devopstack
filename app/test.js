/**
 * @author phuongnd
 */
const axios = require('axios');
const { expect } = require('chai');
/**
 * mocha mocha-test.js --reporter mochawesome csvfilename
 */

let baseurl = "http://localhost:3000"
describe("FTRADING", () => {
    it("HEALTH_CHECK", async () => {
        let data = ({
        });
        let config = {
            method: 'get',
            url: `${baseurl.concat("/hello")}`,
            headers: {},
            data: data
        };
        let response = await axios(config);
        expect(response.status).to.equal(200);
        expect(response.data).contains("SHIBA")
    })
})