# Devops Stack

A devops stack with gitlab EE, sonarqube and kubernetes on CloudVM

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

> 6GB RAM for gitlab server

```

### Provisioning

- On master node: docker swarm init
docker swarm join-token worker

- On worker node: docker swarm join $TOKEN

- Create docker secrets:
    echo "sonar" | docker secret psql_user -

    echo "sonar" | docker secret psql_password -

    echo "123123123" | docker secret gitlab_root_password -

- Create overlay network:
    docker network create --driver overlay --attachable intranet
```

Deploy

```
docker stack deploy -c ./provisioning/docker-compose.yml mystack

```

Monitor

```
- List docker services:
    docker service ls
- View tasks of a service:
    docker service ps $serviceName
- Inspect a service:
    docker service inspect $serviceName
- View service logs:
    docker service logs $serviceName -f


## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```

Give an example

```

### And coding style tests

Explain what these tests test and why

```

Give an example

```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

- [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
- [Maven](https://maven.apache.org/) - Dependency Management
- [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

- **Billie Thompson** - _Initial work_ - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

- Hat tip to anyone whose code was used
- Inspiration
- etc
```
