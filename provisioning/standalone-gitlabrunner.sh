docker run -d --name external_2 --restart always \
    --network mynet \
     -v external2_config:/etc/gitlab-runner \
     -v external2_sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest