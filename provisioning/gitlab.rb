# try out some new config for gitlab behind ssl terminate nginx reverse proxy
# disable gitlab's internal nginx reverse proxy
nginx['proxy_set_headers'] = {
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
}
external_url 'https://infra.servebeer.com'
nginx['listen_port'] = 80
nginx['listen_https'] = false
